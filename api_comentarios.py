# -*- coding: utf-8 -*-
import requests
import json
import datetime
import time
import datetime as t
from bson import json_util

class ApiComments():
    def __init__(self):
        self.url = "https://gorest.co.in/public-api/comments"
       
    def get_data_api(self, page=1):
        api_params = {'page':page}
        #response = requests.get(url = self.url, params = api_params)
        response = requests.get(url = self.url, params = api_params)
        if response != False:
            if response is not None:
                return response.json()
            else:
                return False
        else:
            return False      