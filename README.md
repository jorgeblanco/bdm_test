
# BDM TEST


## Instación
1. Instalar requerimentos `pip install -r requirements.txt`
En nltk se necesitan los siguientes paquetes
En la consola de python ejecutar:
```
import nltk
nltk.download('punkt')
nltk.download('stopwords')
```
2-Montar la base de datos es un motor MySQL.
Crear una base de datos llamada bdm_test.
Ejecutar el archivo bdm_test.sql
## Ejecución:

Ejecutar el archivo comentarios.py desde la consola.
```
$ python3 comentarios.py
```
Desde jupyter
```
!python comentarios.py
```
######
## Nota:
Al ejecutar el archivo comentarios.py y una vez completada todas las peticiones a la api recolecta todos los comentarios y los guarda en una variable llamada "all_data", recorre comentario por comentario y lo guarda en la tabla "comments", paso seguido guarda las 3 palabras con mas frecuencia en el comentario, almacenando el mail, la palabra, la frecuencia y la fecha en la tabla "words_frecuency".
######