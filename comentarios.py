from api_comentarios import ApiComments
from mysql_model import Mysql
from datetime import datetime
from datetime import date
import datetime as t
from config import _config_mysql

#import nltk
from nltk import word_tokenize, FreqDist
from analysis_nltk import Nltk

db = Mysql(_config_mysql)


api = ApiComments()#instancio la clase
today = str(date.today())#fecha de hoy
data_comments = {}#dict para guardar los comentarios de cada uno de los usuarios
all_data = []#lista para guardar los comentarios de todas las peticiones
data = api.get_data_api()#peticion inicial a la api
all_data += data['data']
limit_pages = data['meta']['pagination']['pages'] + 1#le sumo 1 para el range en el for

#Almaceno todos los comentarios
for i in range(2,5):#modificar el limite a limit_pages
    data = api.get_data_api(i)#le paso el numero de la pagina
    all_data += data['data']#almaceno los resultados que trae cada pagina

for one_result in all_data:#recorro todos los resultados
    data_db = {}#dict para guardar cada uno de los registros
    data_db['id'] = one_result['id']
    data_db['mail'] = one_result['email']
    data_db['mensaje'] = one_result['body']
    data_db['fecha_creacion'] = one_result['created_at']
    data_db['fecha_captura'] = today
    print("COMANTARIO")
    print(data_db)
    print('----------------------------------------------------')
    db.insert_comment(data_db)#guardo el comentario en la tabla
    
    #seccion de frecuencia de palabras
    nltk = Nltk()#instancio la clase para realizar la tokenizacion
    string_la = ''#string para guardar los comentario de un usuario, suponiendo que tenga mas de uno en un dia
    words = ''#string para ir guardando las palabras limpias
    string_la += one_result['body']
    token = nltk.tokenize(string_la)#separo palabra por palabra
    for word in token:#limpio el conjunto de palabras
        word = word.lower().strip()#a minusculas y remuevo los espacios
        if not(word in nltk.descart):
            word = word + ' '
            words += word
    freq_words = FreqDist(nltk.tokenize(words))
    data_word = {}#dict para almacenar las palabras con su frecuencia de cada mail
    data_word['mail'] = one_result['email']
    data_word['fecha'] = today
    for list_freq in freq_words.most_common(3):#solo tomo las 3 con mas frecuencia
        print('PALABRA: ',str(list_freq[0]),' FRECUENCIA: ', str(list_freq[1]))
        data_word['palabra'] = list_freq[0]
        data_word['frecuencia'] = list_freq[1]
        db.insert_words_frecuency(data_word)
    print('--------------------------------------------------------------------------------------------------------------')
print('Datos Insertados Correctamente: '+str(t.datetime.now()))
db.commit_and_close_operation()#confirmo las inserciones y cierro la conexion


