#!/usr/bin/env python
# -*- coding: utf-8 -*-
import nltk
import os
import io
from nltk.corpus import stopwords
from nltk import word_tokenize, FreqDist
from config import _config_mysql
from mysql_model import Mysql
from nltk.text import Text

class Nltk():
    descart = set(stopwords.words('spanish'))
    descart.update(['k','x','q','...','va','vez','lugar','quiero','abrazo','hace',
            'vamos','años','si','ser','10','san','hoy','``','\'\'','cada','día', 'puede', 
            'merece', '.', ',','#',')','(','?', '\'', ':', 'más', '!','/', 'de', 'toda','tn',
            '&','ingresá','ver','50mil','xq','cn', 'sr', '.','aca', 'vo', 'h','t','re','asi','así','dia','tan','..'
            ,'dr','vos','mas','dar','siga','ver','que','y','de','no',';','mejor','mucha','necesitan','ahora','ojala','hacer','quieren','mal','sol','jaja','cosa','sos','mano'
            , 'mismo','todas','allá', 'saben','lado','creo','habla','solo','jajaja','','cumple','cumpleaños','feliz','gobernador', 'espero','parece','nadie','casa','tambien','ojalá', 'van','acá','gustavo','señora','quiere', 'paso','chico','unico','dice','ruiz','diaz','ahi','ahí','inocencia','provincia','hacen','gran','luis','comentario','pablo', 'mendez','maria','sergio','miguel','angel','tener', 'despues','pueden', 'mayra', 'ja', 'sebastián','isabel', 'fernandez','marcos','dan','tal','etc','nombre','desodorante'])
    dir_word_analysis = '../word_analysis/'

    def tokenize(self, p_string):
        return word_tokenize(p_string)

    def concordance(self, text, word):
        return text.concordance(word)
